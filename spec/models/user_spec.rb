require 'rails_helper'

RSpec.describe User, :type => :model do
  subject {
    described_class.new(password: '123456', email: 'k@gmail.com', name: 'karl', password_confirmation: '123456', created_at: '2021-03-10 13:19:16.236753', updated_at: '2021-03-10 13:19:16.236753',)
  }
  describe 'Validations' do
    it 'is valid with valid attributes' do
      expect(subject).to be_valid
    end
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to have_secure_password }
    it { is_expected.to validate_presence_of(:code) }
    it 'is not valid with an invalid email' do
      subject.email = "nil"
      expect(subject).to_not be_valid
    end
  end

  describe 'Column Specification' do
    it { should have_db_column(:email).of_type(:string) }
    it { should have_db_column(:name).of_type(:string) }
    it { should have_db_column(:created_at).of_type(:datetime) }
    it { should have_db_column(:updated_at).of_type(:datetime) }
  end
end
