require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MyCookBookApp
  class Application < Rails::Application
    config.load_defaults 6.0
    config.generators.helper = false
    Bundler.require(*Rails.groups)
    Dotenv::Railtie.load
    HOSTNAME = ENV['HOSTNAME']
  end
end
